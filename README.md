# README #

Dropdown плагин. Реагирует на tab и esc. При попадании фокуса на `.dropdown` показывается меню `.dropdown-options`

### SCSS ###

Создайте собственные стили. При открытии выпадающего списка добавляется класс `.dropdown-open`.

```scss
.dropdown
{
	// ...

    $root: &;
    &.dropdown__open #{$root}-options {
        touch-action: auto;
        transform: translateY(0) scale(1);
        opacity: 1;
        z-index: 1;
    }
	
	// ...
}
```

### HTML ###

Шаблон html

```html
<div class="dropdown">
	<!-- кнопка `dropdown-toggler` -->
    <a href="javascript:void(0);" class="dropdown-toggler">
        <span class="dropdown-toggler__icon">
            <i class="fas fa-bell"></i>
        </span>
		<span class="dropdown-toggler__text">Уведомления</span>
    </a>
	
	<!-- выпадающий список -->
    <div class="dropdown-options">
        <a href="javascript:void(0);">Пополнить счет</a>
        <a href="javascript:void(0);">Документы</a>
		<div class="dropdown-separator"></div>
		<a href="javascript:void(0);">Выход</a>
    </div>
</div>
```