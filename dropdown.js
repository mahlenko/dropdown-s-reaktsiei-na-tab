/**
 * Dropdown options
 * @type {*|jQuery.fn.init|jQuery|HTMLElement}
 */
let __dropdown_classes = {
    dropdown: '.dropdown',
    options:  '.dropdown-options',
    toggler:  '.dropdown-toggler',
    open:     'dropdown__open'
};

var $_activeDropdown;

/**
 *
 * @type {{init: Window.dropdown.init, close: Window.dropdown.close, open: Window.dropdown.open}}
 */
window.dropdown = {
    init: function() {
        $.each($(__dropdown_classes.dropdown), function() {
            $(this).find('*').focusin((e) => {
                window.dropdown.open($(e.target).parents(__dropdown_classes.dropdown));
            });
        });
    },

    open: function (element) {
        $_activeDropdown = $(element);
        setTimeout(() => {
            $_activeDropdown.addClass(__dropdown_classes.open);
        }, 0)
    },

    close: function(e) {
        if ($_activeDropdown)
        {
            if ((e.keyCode && e.keyCode === 27) || $_activeDropdown.has($(':focus')).length === 0 || (e.type === 'click' && $_activeDropdown.find(__dropdown_classes.options).has(e.target).length))
            {
                $_activeDropdown = undefined;
                $(__dropdown_classes.dropdown).removeClass(__dropdown_classes.open);

                $(':focus').blur()
            }
        }
    }
};

/**
 *
 */
$(document).find('*').on('focusout click keyup', function(e) {
    window.dropdown.close(e);
});

/**
 *
 */
$(document).ready(function() {
    window.dropdown.init();
});
